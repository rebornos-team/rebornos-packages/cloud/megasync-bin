# megasync-bin

Easy automated syncing between your computers and your MEGA cloud drive

https://mega.io/linux/MEGAsync/Arch_Extra/x86_64/

How to clone this repository:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/cloud/megasync-bin.git
```
